#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


#include "test_ioctl.h"

int main(int argc, char **argv)
{

    int fd;
    int ret;
    //struct msg my_msg;
    char data[20]={0};
    //int data=412;
    fd = open("/dev/ioctl_test", O_RDWR);
    if (fd < 0) {
        perror("open");
        exit(-2);
    }

    /* 初始化设备 */
    ret = ioctl(fd, IOCTL_INIT);
    if (ret) {
        perror("ioctl init:");
        exit(-3);
    }

    /* 往设备文件写入数据 */
    memset(&data, 0, sizeof(data));
    strcpy(data,"hello,world");
    ret = ioctl(fd, IOCTL_WRITE_REG, &data);
    if (ret) {
        perror("ioctl write fail:");
        exit(-4);
    }
    printf("send :%s\n",data);

    /* 向设备文件读取数据 */
    memset(&data, 0, sizeof(data));
    //data=0;
    ret = ioctl(fd, IOCTL_READ_REG, &data);
    if (ret) {
        perror("ioctl read fail:");
        exit(-5);
    }
    printf("read: %s\n",data);

    return 0;
}
