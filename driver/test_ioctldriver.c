#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <linux/fs.h>        // file_operation is defined in this header
#include <linux/device.h>
#include <linux/scpi_protocol.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
//#include <uapi/asm-generic/ioctl.h>
#include "test_ioctl.h"

static int majorNumber;
static struct class*  test_module_class = NULL;
static struct device* test_module_device = NULL;


//define device name
#define DEVICE_NAME "ioctl_test"
#define CLASS_NAME "test_module"

//函数原型

static long test_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static int test_init(void);
static void test_exit(void);

static const struct file_operations fops = {
    .owner = THIS_MODULE,
    //.open = test_open,
    //.release = test_close,
    //.read = test_read,
   // .write = etst_write,
    .unlocked_ioctl = test_ioctl,
};


static long test_ioctl(struct file *file, unsigned int cmd,unsigned long arg)
{
    printk("[%s]\n", __func__);

    int ret;
    //struct msg my_msg;
    char data[20]={0};
    //int data=0;
    /* 检查设备类型 */
    if (_IOC_TYPE(cmd) != IOC_MAGIC) {
        pr_err("[%s] command type [%c] error!\n",  __func__, _IOC_TYPE(cmd));
        return -ENOTTY;
    }

    /* 检查序数 */
    if (_IOC_NR(cmd) > IOC_MAXNR) {
        pr_err("[%s] command numer [%d] exceeded!\n",__func__, _IOC_NR(cmd));
        return -ENOTTY;
    }

    /* 检查访问模式 */
    if (_IOC_DIR(cmd) & _IOC_READ)
        ret= !access_ok((void __user *)arg,  _IOC_SIZE(cmd));
        //ret= !access_ok(VERIFY_WRITE, (void __user *)arg,  _IOC_SIZE(cmd));
    else if (_IOC_DIR(cmd) & _IOC_WRITE)
        ret= !access_ok((void __user *)arg,  _IOC_SIZE(cmd));
        //ret= !access_ok(VERIFY_READ, (void __user *)arg,  _IOC_SIZE(cmd));
    if (ret)
        return -EFAULT;

	/* 命令实现 */
    switch(cmd) {
	    /* 初始化设备 */
	    case IOCTL_INIT:
	        printk("start!!!\n");
	        break;

	    /* 核外程序向设备读取数据，核内写入数据 */
	    case IOCTL_READ_REG:
	        //data=812;
	        memset(&data, 0, sizeof(data));
            strcpy(data,"ok,fine");
	        ret = copy_to_user((char *)arg, &data, sizeof(data));
	        if (ret!=0)
	            return -EFAULT;
	        printk("send data：%s\n",data);
	        break;

	    /* 核外程序向设备写入数据，核内读取数据 */
	    case IOCTL_WRITE_REG:
            //memset(&data, 0, sizeof(data));
            //strcpy(data,"ok,fine");
	        ret = copy_from_user(&data, (char *)arg, sizeof(data));
	        if (ret!=0)
	            return -EFAULT;
            printk("read data：%s\n",data);
	        break;

	    default:
	        return -ENOTTY;
    }

    return 0;
}

static int test_init(void){

        printk(KERN_INFO "[TestModule:] Entering test module. \n");
        // 在加载本模块时，首先向操作系统注册一个chrdev，也即字节设备，三个参数分别为：主设备号（填写0即为等待系统分配），设备名称以及file_operation的结构体。返回值为系统分配的主设备号。
        majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
        if(majorNumber < 0){
                printk(KERN_INFO "[TestModule:] Failed to register a major number. \n");
                return majorNumber;
        }
        printk(KERN_INFO "[TestModule:] Successful to register a major number %d. \n", majorNumber);

        //接下来，注册设备类
        test_module_class = class_create(THIS_MODULE, CLASS_NAME);
        if(IS_ERR(test_module_class)){
                unregister_chrdev(majorNumber, DEVICE_NAME);
                printk(KERN_INFO "[TestModule:] Class device register failed!\n");
                return PTR_ERR(test_module_class);
        }
        printk(KERN_INFO "[TestModule:] Class device register success!\n");

        //最后，使用device_create函数注册设备驱动
        test_module_device = device_create(test_module_class, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
        if (IS_ERR(test_module_device)){               // Clean up if there is an error
                class_destroy(test_module_class);           // Repeated code but the alternative is goto statements
                unregister_chrdev(majorNumber, DEVICE_NAME);
                printk(KERN_ALERT "Failed to create the device\n");
                return PTR_ERR(test_module_device);
        }
        printk(KERN_INFO "[TestModule:] Test module register successful. \n");
        return 0;

}


static void test_exit(void)
{

		//退出时，依次清理生成的device, class和chrdev。这样就将系统/dev下的设备文件删除，并自动注销了/proc/devices的设备。
        printk(KERN_INFO "[TestModule:] Start to clean up module.\n");
        device_destroy(test_module_class, MKDEV(majorNumber, 0));
        class_destroy(test_module_class);
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_INFO "[TestModule:] Clean up successful. Bye.\n");

}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("zhubing");
MODULE_DESCRIPTION("Driver as a test case");
