#ifndef _TEST_H_
#define _TEST_H_

#include <linux/ioctl.h>	/* 内核空间 */

/* 定义幻数 */
#define IOC_MAGIC 'c'

/* 定义命令 */
/* 初始化设备 */
#define IOCTL_INIT _IO(IOC_MAGIC, 0)
/* 读寄存器 */
#define IOCTL_READ_REG _IOR(IOC_MAGIC, 1, char *)
/* 写寄存器 */
#define IOCTL_WRITE_REG _IOW(IOC_MAGIC, 2, char *)

#define IOC_MAXNR 3

/* 设备描述结构体 */
/*
struct msg {
	int addr;
	unsigned char data[20];
};
*/
#endif
